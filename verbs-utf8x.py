# -*- coding: utf-8 -*-
from pylab import *
from matplotlib.font_manager import FontProperties
from random import Random
import matplotlib.axes3d as p3
import pylab

#------------------------------------------------------------------------------#
#---------------    Erez Lieberman, Jean-Baptiste Michel   --------------------#
#---------------     Program for Evolutionary Dynamics     --------------------#
#---------------    Figures for publication, 18 Feb 2007   --------------------#
#------------------------------------------------------------------------------#
# modified and extended by Phillip Alday, May 2010

'''

	Usage: either run the script with the F5 key, or type in the command  'execute()'
	Input: the table of verbs and their annotations (see filename, below)
	Output: the three figures analyzing the regularization on the dataset provided,
		as well as .tex files showing the data in tabular format. The Verbs Table
		shows a classification of each verb relatively to its frequency and
		whether it is irregular, along with the half lives of the frequency class
		verbs belong to.



    ----------------------------------------------------------------------------


	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

'''
#------------------------------------------------------------------------------#
#---------------------------   PARAMETERS  ------------------------------------#
#------------------------------------------------------------------------------#

#filename = "english_verbs.txt"
# filename = "Lieberman-Michel_english_verbs.txt"             # Input
# fileTexVerbsTable = "english-verbs-table.tex";     # Output
# fileTexSuppTable = "english-verbs-supp.tex";
# figure1 = 'english-figure1.png';
# figure2 = 'english-figure2.png';
# figure3 = 'english-figure3.png';
# glanguage = 'englisch';
# elanguage = 'English';

# filename = "german-inputs.txt";
# fileTexVerbsTable = "german_verbs_table.tex";     # Output
# fileTexSuppTable = "german_verbs_supp.tex";
# figure1 = 'german-figure1.png';
# figure2 = 'german-figure2.png';
# figure3 = 'german-figure3.png';
# glanguage = 'hochdeutsch';
# elanguage = 'German';

# filename = "german-inputs-ult.txt";
# fileTexVerbsTable = "german-verbs-table-ult.tex";     # Output
# fileTexSuppTable = "german-verbs-supp-ult.tex";
# figure1 = 'german-ult-figure1.png';
# figure2 = 'german-ult-figure2.png';
# figure3 = 'german-ult-figure3.png';
# glanguage = 'hochdeutsch';
# elanguage = 'German';

# filename = "german-inputs-muzzy-reg.txt";
# fileTexVerbsTable = "german_verbs_table-muzzy-reg.tex";     # Output
# fileTexSuppTable = "german_verbs_supp-muzzy-reg.tex";
# figure1 = 'german-muzzy-reg-figure1.png';
# figure2 = 'german-muzzy-reg-figure2.png';
# figure3 = 'german-muzzy-reg-figure3.png';
# glanguage = 'hochdeutsch';
# elanguage = 'German';

# filename = "german-inputs-praet-reg.txt";
# fileTexVerbsTable = "german_verbs_table-praet-reg.tex";     # Output
# fileTexSuppTable = "german_verbs_supp-praet-reg.tex";
# figure1 = 'german-praet-reg-figure1.png';
# figure2 = 'german-praet-reg-figure2.png';
# figure3 = 'german-praet-reg-figure3.png';
# glanguage = 'hochdeutsch';
# elanguage = 'German';

# filename = "german-inputs-alle-reg.txt";
# fileTexVerbsTable = "german_verbs_table-alle-reg.tex";     # Output
# fileTexSuppTable = "german_verbs_supp-alle-reg.tex";
# figure1 = 'german-alle-reg-figure1.png';
# figure2 = 'german-alle-reg-figure2.png';
# figure3 = 'german-alle-reg-figure3.png';
# glanguage = 'hochdeutsch';
# elanguage = 'German';

# filename = "german-inputs-summe-freq.txt";
# fileTexVerbsTable = "german_verbs_table-summe-freq.tex";     # Output
# fileTexSuppTable = "german_verbs_supp-summe-freq.tex";
# figure1 = 'german-summe-freq-figure1.png';
# figure2 = 'german-summe-freq-figure2.png';
# figure3 = 'german-summe-freq-figure3.png';
# glanguage = 'hochdeutsch';
# elanguage = 'German';

filename = "german-inputs-hoechst-freq.txt";
fileTexVerbsTable = "german_verbs_table-hoechst-freq.tex";     # Output
fileTexSuppTable = "german_verbs_supp-hoechst-freq.tex";
figure1 = 'german-hoechst-freq-figure1.png';
figure2 = 'german-hoechst-freq-figure2.png';
figure3 = 'german-hoechst-freq-figure3.png';
glanguage = 'hochdeutsch';
elanguage = 'German';


axeslabelsize = 14;
legendfontsize = 13;
xticklabelsize = 12;
yticklabelsize = 12;

DISPLAY = True;             # Display the figures?
SAVE = True;                # Save the figures?
# dpi = 600;
fig1size = figsize=(8,9);
fig2size = figsize=(8,10);
fig3size = figsize=(9,9);
dpi = 300;
# fig1size = figsize=(4,4.5);
# fig2size = figsize=(4,5);
# fig3size = figsize=(4.5,4.5);


numBins=6                   # Number of bins for the histogram
begDecay = 0                # Parameters for where we start/end plotting the decay (id of bins)
endDecay = 4;
begFreq = 0;                # Parameters for where we start/end plotting the frequencies in panel 2
endFreq=4;
begEvol=1;                  # Parameters for where we start/end fitting the original Zipf law (panel3 3)
endEvol=7;

years = [800, 1100, 2000];  # Dates of Old lish, Middle lish, Modern lish
yearsstring = [' 800', ' 1200', ' 2000'];
sizeCorpus = -1             # Size of the corpus - to be initialized with the data

inf = log10(0);             # The "Infinite number"
endTimes = 2500;            # The last time point to compute for panel 3
samplingOfFrequency = 20;   # Number of frequency points to compute the 3D mesh for
samplingOfTime = 30;        # Number of time points to compute the 3D mesh for

markersize = 6              # Size of the markers on the graphs
angle3_1 = (-16.42, 14.68)  # A possible view angle for panel 3
angle3_2 = (41.39, 15.60)   # Another possible view angle for panel 3
angle3_3 = (13.20, 21.78)
angle3_4 = (14.34, 25.17)
angle2_1 = (-47.27, 10.73)  # A possible view angle for panel 2, figure 1
angle2_2 = (-26.13, 4.83)   # Another possible view angle for panel 2, figure 1
angle2_3 = (-21.04, 3.98)   # Another possible view angle for panel 2, figure 1
angle2_4 = (-30.4, 22.59)
angle2_5 = (-25.56, 20.17)

angle3 = angle3_3           # Final angles
angle2 = angle2_5

numLinesSuppTable = 36      # Number of lines on a single page for the latex table

#------------------------------------------------------------------------------#
#--------------------------   MAKING PANELS  ----------------------------------#
#------------------------------------------------------------------------------#


def execute():
    # Launches the analysis
    makeAllPanels()


def makeAllPanels():
    # Creates all the panels
    makeFirstPanel(SAVE, False)
    makeSecondPanel(SAVE, False)
    makeThirdPanel(SAVE, False)
    makeResultFile()

    if (DISPLAY):
        show()


def makeFirstPanel(toSave=SAVE, toDisplay=DISPLAY):
    # Creates the first panel

    print("Making first figure..")
    res = readVerbsFile(filename);
    sourced = getSourcedIrregulars(res)
    xlims = [0.000002, 0.3]
    figure(figsize=fig1size, facecolor='w')

    plotTangDistributions(sourced, xlims);  # The Tang distributions are the distributions of irregular verbs
                                            # according to their frequencies
    plotDecayRates(sourced, xlims)

    if (toDisplay):
        show()
    if (toSave):
        figname = figure1
        savefig(figname, dpi=dpi)
        print('Figure saved in ' + figname)
    print("\n")



def makeSecondPanel(toSave=SAVE, toDisplay=DISPLAY):
    # Creates the second panel

    print("Making second figure..")
    res = readVerbsFile(filename);
    sourced = getSourcedIrregulars(res)

    [frequencies, dum, dum, dum] = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequencies);
    tang = getTangDistributions(edges, sourced)
    bins = getBinning(edges, frequencies)

    print(" ")
    print("The irregular verb distributions are:")
    print("Modern: " + str(tang[0]))
    print("Middle: " + str(tang[1]))
    print("Old:    " + str(tang[2]))


    fig = figure(figsize=fig2size, facecolor='w')
    ax = plot3DIrregularDistributions(sourced, fig)
    plotDecayFromFrequencies(sourced, fig)
    if (toDisplay):
        show()
    if (toSave):
        figname = figure2
        savefig(figname, dpi=dpi)
        print('Figure saved in ' + figname)
    print("\n")
    return ax



def makeThirdPanel(toSave=SAVE, toDisplay=DISPLAY):
    # Creates the third panel

    print("Making third figure..")
    res = readVerbsFile(filename);
    sourced = getSourcedIrregulars(res)

    yearsToTry = linspace(0, 2000, 20);
    realYears = zeros(3)
    realYears[0] = years[2] ;
    realYears[1]= years[1] ;
    realYears[2] = years[0] ;

    fig = figure(figsize=fig3size,facecolor='w')
    ax = plotTheBestMesh(sourced, yearsToTry, realYears, fig)
    plotTimeSlicesOverMesh(sourced, ax)

    if (toDisplay):
        show()
    if (toSave):
        figname = figure3
        savefig(figname, dpi=dpi)
        print('Figure saved in ' + figname)
    print("\n")



def makeResultFile():
    # Saves the results in a file

    print("Making results file..")
    print(" ")
    res = readVerbsFile(filename);
    sourced = getSourcedIrregulars(res)
    printStatistics(sourced)
    [frequencies, dum, dum, dum] = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequencies);
    [freqSlices, [dum, dum, dum, decayExponent, decayIntercept], bars] = getDecayFromFrequencySlices(edges, centers, sourced)
    tang = getTangDistributions(edges, sourced)
    bins = getBinning(edges, frequencies)

    print(" ")
    print("The irregular verb distributions are:")
    print("Modern: " + str(tang[0]))
    print("Middle: " + str(tang[1]))
    print("Old:    " + str(tang[2]))

    print(" ")
    fut = predictNumerosity(centers, tang[0], decayExponent, decayIntercept, 500)
    print("Among the " + str(sum(tang[0])) + " irregulars verbs in Modern " + elanguage + " used in this study, only " + str(int(fut)) + " will remain irregular in 500 years from now")
    fut = predictNumerosity(centers, tang[0], decayExponent, decayIntercept, 1000)
    print("Among the " + str(sum(tang[0])) + " irregulars verbs in Modern " + elanguage + " used in this study, only "  + str(int(fut)) + " will remain irregular in 1000 years from now")

    print(" ")
    n = getEquivalentDecay(decayExponent, decayIntercept, centers[4], centers[2], 10, 10)
    print("\n")
    print("Let us take ten verbs from the third most frequent bin, and ten from the fifth most frequent bin.")
    print("When one verb from the 10 most frequent will have regularized, " + str(n)[:3] + " verbs from the 10 least frequent will also have regularized.")

    writeTexVerbsTable(fileTexVerbsTable, sourced, bins, decayExponent, decayIntercept, centers)
    writeTexSupplementaryTable(fileTexSuppTable, res)


#------------------------------------------------------------------------------#
#-----------------------------   SIMPLE STATISTICS  ---------------------------#
#------------------------------------------------------------------------------#


def printStatistics(res):
    # We simply print out the basic statistics for the input verb file - how many irregulars, etc

    r = getStatistics(res);

    print(str(r[0]) + " were read. Among those:")
    print(str(r[1]) + " are irregular in Modern " + elanguage + ", ");
    print(str(r[2]) + " are irregular in Middle " + elanguage + ", ");
    print(str(r[3]) + " are irregular in Old " + elanguage + ".");


def getStatistics(res):
    # Returns the basic statistics of the set of irregular verbs.
    # 'res' is the file returned by readVerbsFile

    numVerbs = len(res);
    numModernIrregs = 0;
    numMiddleIrregs = 0;
    numOldIrregs = 0;

    for (v, f, imod, imid, iold, smid, sold) in res:
        numModernIrregs = numModernIrregs + imod;
        numMiddleIrregs = numMiddleIrregs + imid;
        numOldIrregs = numOldIrregs + iold;
    return [numVerbs, numModernIrregs, numMiddleIrregs, numOldIrregs];


def getDescendantStatistics(res):
    # Returns the statistics of the descendant of old irregulars

    numVerbs = len(res);
    numModernIrregs = 0;
    numMiddleIrregs = 0;
    numOldIrregs = 0;

    for (v, f, imod, imid, iold, smid, sold) in res:
        numModernIrregs = numModernIrregs + imod*iold*imid;
        numMiddleIrregs = numMiddleIrregs + imid*iold;
        numOldIrregs = numOldIrregs + iold;
    return [numVerbs, numModernIrregs, numMiddleIrregs, numOldIrregs];





#------------------------------------------------------------------------------#
#--------------------------- RAW DATA EXTRACTION   ----------------------------#
#------------------------------------------------------------------------------#


def getTangDistributions(bins, sourcedIrregulars):
    # This function directly fetches from the data the Tang distributions that we can
    # see in Figure 1A. The code is straightforward

    [dum, freqsModern, freqsMiddle, freqsOld] = getFrequencies(sourcedIrregulars)

    h1 = produceHistogram(bins, freqsModern);
    h2 = produceHistogram(bins, freqsMiddle);
    h3 = produceHistogram(bins, freqsOld);

    return [h1, h2, h3];


def get3DIrregularDistribution(edges, centers, sourcedIrregulars):
    # This function performs the same task as the previous one, in a 3d version.
    # Note that the scales, though, are logarithmic in frequency and numerosity,
    # and linear in time.

    [h1, h2, h3] = getTangDistributions(edges, sourcedIrregulars)

    l1 = zeros(len(h1), dtype=float);
    l2 = zeros(len(h1), dtype=float);
    l3 = zeros(len(h1), dtype=float);

    yold = zeros(len(h1))
    ymid = zeros(len(h2))
    ymod = zeros(len(h2))

    l = zeros(len(h1), dtype=float);

    i = 0;
    for (v1, v2, v3) in zip(h1, h2, h3):
        l1[i] = log10(v1)
        l2[i] = log10(v2);
        l3[i] = log10(v3);
        yold[i] = years[0]
        ymid[i] = years[1]
        ymod[i] = years[2]
        l[i] = log10(centers[i])
        i = i+1


    old = [yold, l, l3];
    middle = [ymid, l, l2];
    modern = [ymod, l, l1];

    return [modern, middle, old]



def getSourcedIrregulars(res):
    # returns the verbs that have sources for OE and ME, and that are irregular
    # in OE. Those verbs are the ones that we will use in the study.

    sourced = [];
    for (v, freq, imod, imid, iold, sold, smid) in res:
        if (iold==1):
            sourced.append((v, freq, imod, imid, iold, sold, smid))


    return sourced

def getSourcedIrregularsFromClass(res, classes, numclass):
    # returns the verbs that have sources for OE and ME, and that are irregular
    # in OE. Those verbs are the ones that we will use in the study.

    sourced = [];
    for (v, freq, imod, imid, iold, sold, smid) in res:
        if (iold==1 and classes[v]==numclass):
            sourced.append((v, freq, imod, imid, iold, sold, smid))
            print(v)
    return sourced


def getFrequencies(res):
    # Extracts te frequencies from the results file. This is the data used in the study.

    stats = getDescendantStatistics(res);
    freqs = zeros(stats[0], dtype=float);
    freqsModern = zeros(stats[1], dtype=float);
    freqsMiddle = zeros(stats[2], dtype=float);
    freqsOld = zeros(stats[3], dtype=float);
    i = 0;
    for (v, freq, imod, imid, iold, smid, sold) in res:
        freqs[i] = freq;
        i = i+1;
    i = 0;
    for (v, freq, imod, imid, iold, smid, sold) in res:
        if (iold==1):
            freqsOld[i] = freq;
            i = i+1;
    i = 0;
    for (v, freq, imod, imid, iold, smid, sold) in res:
        if (iold*imid == 1):
            freqsMiddle[i] = freq;
            i = i+1;
    i = 0;
    for (v, freq, imod, imid, iold, smid, sold) in res:
        if (iold*imod*imid == 1):
            freqsModern[i] = freq;
            i = i+1;

    return [freqs, freqsModern, freqsMiddle, freqsOld];



#------------------------------------------------------------------------------#
#--------------------------    HISTOGRAMS    ----------------------------------#
#------------------------------------------------------------------------------#


def produceHistogram(edges, frequs):
    # A code for computing the histogram of a distribution - frequs - knowing the edges
    # for the histogram.

    hist = zeros(len(edges) - 1, dtype=float);
    i = 0;
    for (minu, maxi) in zip(edges[:len(edges)-1], edges[1:]):
        K = find(frequs>=minu)
        J = find(frequs>maxi)
        hist[i] = len(K)- len(J);
        i = i+1
    return hist



def getBinning(edges, frequs):
    # Here we assign to each frequency its bin within the histogram. The output
    # is an array of same size as frequs, with the number of the bin the frequency
    # belongs to.

    bins = zeros(len(frequs));
    for i,f in enumerate(frequs):
        K = find(edges <= f)
        bins[i] = max([0, len(K)-1])

    return bins



def produceErrorsHistogram(edges, frequsPlus, frequsMinus):
    # We produce the error bars for the histograms by computing the histograms in the worst cases,
    # given the boundaries of the error frequsPlus, frequsMinus

    hist = zeros(len(edges) - 1, dtype=float);
    i = 0;
    for (minu, maxi) in zip(edges[:len(edges)-1], edges[1:]):
        K = find(frequsPlus>minu)
        J = find(frequsMinus>maxi)
        hist[i] = len(K)- len(J);
        i = i+1
    return hist



def getHistogramParameters(frequs, correctEdges=False, posToCorrect=[0], valueToCorrect=[0]):
    # Returns the edges that we will use in the histogram. Those are computed using
    # the simplest binning, namely linear in a logarithmic space.

    K = find(frequs>0)
    v = linspace(log10(min(frequs[K])), max(log10(frequs[K])), numBins+1)

    edges = v;
    edges[0] = log10(min(frequs[K])*0.99)       # this is just to make sure the min and max
    edges[numBins] = log10(max(frequs[K])*1.01) # verbs are counted.
    if correctEdges:
        for (posi, valu) in zip(posToCorrect, valueToCorrect):
            edges[posi] = valu;


    centers = zeros(len(edges)-1, dtype=float);
    i = 0
    for (minu, maxi) in zip(v[:len(v)-1], v[1:]):
        centers[i] = (maxi+minu)/2;      # logarithmic mean
        i = i+1

    return [10**edges, 10**centers]





#------------------------------------------------------------------------------#
#---------------------------  COMPUTATION  ------------------------------------#
#------------------------------------------------------------------------------#


def computeTangErrors(bins, sourcedIrregulars):
    # We compute the error bars for the Tang distributions. Those are errors due
    # to a Poisson noise in the measurement in the frequencies of the verbs

    # we compute the measurement errors for each frequency
    [freqsErrPlus,freqsErrMinus, freqsModernErrPlus,freqsModernErrMinus, freqsMiddleErrPlus,freqsMiddleErrMinus, freqsOldErrPlus, freqsOldErrMinus] = computeFrequenciesErrors(sourcedIrregulars)

    # We produce the histograms errors
    he1 = produceErrorsHistogram(bins, freqsModernErrPlus, freqsModernErrMinus);
    he2 = produceErrorsHistogram(bins, freqsMiddleErrPlus, freqsMiddleErrMinus);
    he3 = produceErrorsHistogram(bins, freqsOldErrPlus, freqsOldErrMinus);

    bars1 = he1
    bars2 = he2
    bars3 = he3

    [h1, h2, h3] = getTangDistributions(bins, sourcedIrregulars);
    i=0;
    for (e1, e2, e3, v1, v2, v3) in zip(he1, he2, he3, h1, h2, h3):
        bars1[i] = float(e1-v1)/2;  # the error is the distance between the Plus and Minus histograms
        bars2[i] = float(e2-v2)/2;
        bars3[i] = float(e3-v3)/2;
        i = i+1

    return [bars1, bars2, bars3];



def computeFrequenciesErrors(res):
    # We compute the poisson error of frequency measurement.
    # These will generate the error bars

    [freqs, freqsModern, freqsMiddle, freqsOld] = getFrequencies(res)
    freqsErrPlus = freqs;
    freqsModernErrPlus = freqsModern
    freqsMiddleErrPlus = freqsMiddle
    freqsOldErrPlus = freqsOld

    freqsErrMinus = freqs;
    freqsModernErrMinus = freqsModern
    freqsMiddleErrMinus = freqsMiddle
    freqsOldErrMinus = freqsOld

    for i, f in enumerate(freqs):
        freqsErrPlus[i] = f+sqrt(f/sizeCorpus)
        freqsErrMinus[i] = f-sqrt(f/sizeCorpus)
    for i, f in enumerate(freqsModern):
        freqsModernErrPlus[i] = f+sqrt(f/sizeCorpus)
        freqsModernErrMinus[i] = f-sqrt(f/sizeCorpus)
    for i, f in enumerate(freqsMiddle):
        freqsMiddleErrPlus[i] = f+sqrt(f/sizeCorpus)
        freqsMiddleErrMinus[i] = f-sqrt(f/sizeCorpus)
    for i, f in enumerate(freqsOld):
        freqsOldErrPlus[i] = f+sqrt(f/sizeCorpus)
        freqsOldErrMinus[i] = f-sqrt(f/sizeCorpus)

    return [freqsErrPlus,freqsErrMinus, freqsModernErrPlus,freqsModernErrMinus, freqsMiddleErrPlus,freqsMiddleErrMinus, freqsOldErrPlus, freqsOldErrMinus];


def stdev(a):
    return sqrt(mean(a*a) - mean(a)**2)


def computeTangSamplingError(edgess, centerss, sourced):
    r = Random(10)
    xlims = [0.000002, 0.3]

    numtests = 20

    decayExpOld = zeros(numtests, dtype=float)
    decayExpMid = zeros(numtests, dtype=float)
    decaysOld = zeros((numtests,numBins), dtype=float)
    decaysMid = zeros((numtests,numBins), dtype=float)
    tangOld = zeros((numtests,numBins), dtype=float)
    tangMid = zeros((numtests,numBins), dtype=float)
    tangMod = zeros((numtests,numBins), dtype=float)
    slopesOfFreqSlices = zeros((numtests,numBins), dtype=float)

    errorTangOld = zeros(numBins, dtype=float)  # Error bars on Tang distributions
    errorTangMid = zeros(numBins, dtype=float)
    errorTangMod = zeros(numBins, dtype=float)
    errorDecaysOld = zeros(numBins, dtype=float) # Error bars on the decay plots
    errorDecaysMid = zeros(numBins, dtype=float)
    errorSlopesOfFreqSlices = zeros(numBins, dtype=float) # Error bars on the time-dependant decay
    meanDecayOld = 0.0  # errors on the value of the exponent
    stdevDecayOld = 0.0
    meanDecayMid = 0.0
    stdevDecayMid = 0.0



    for i in arange(0, numtests):

        # Bootstrap
        s = sourced[:]
        for k in arange(1, int(float(size(sourced))/70)):
            s.pop(r.randrange(0, size(s)/7))

        # Tang distributions
        frequs = getFrequencies(s);
        [edges, centers] = getHistogramParameters(frequs[0]);
        tang = getTangDistributions(edges, s)

        tangOld[i, :] = tang[2]
        tangMid[i,:] = tang[1]
        tangMod[i,:] = tang[0]

        decay = computeDecayRates(edges, centers, s);
        decayExpOld[i] = decay[6]
        decayExpMid[i] = decay[7]
        decaysOld[i,:] = decay[0]
        decaysMid[i,:] = decay[1]

        [dum, [slopesOfFreqSlices[i,:], dum, dum, dum, dum]] = computeDecayFromFrequencySlices(centers, tang)


    for i in arange(0, numBins):
        errorTangOld[i] = stdev(tangOld[:,i])
        errorTangMid[i] = stdev(tangMid[:,i])
        errorTangMod[i] = stdev(tangMod[:,i])
        errorDecaysOld[i] = stdev(decaysOld[:,i])
        errorDecaysMid[i] = stdev(decaysMid[:,i])
        errorSlopesOfFreqSlices[i] = stdev(slopesOfFreqSlices[:,i])

    meanDecayOld = mean(decayExpOld)
    meanDecayMid = mean(decayExpMid)
    stdevDecayOld = stdev(decayExpOld)
    stdevDecayMid = stdev(decayExpMid)

    return (errorTangOld, errorTangMid, errorTangMod, errorDecaysOld, errorDecaysMid,
            meanDecayOld, meanDecayMid, stdevDecayOld, stdevDecayMid, errorSlopesOfFreqSlices)



def computeDecayRates(bins,centers, sourcedIrregulars):
    # We compute the decay rates in a time-independant fashion, between the Old and Modern variants, and
    # between the Middle and Modern variants.

    [h1, h2, h3] = getTangDistributions(bins, sourcedIrregulars);
    [e1, e2, e3] = computeTangErrors(bins, sourcedIrregulars);

    decayOldModern = zeros(len(h1), dtype=float)
    decayMiddleModern = zeros(len(h1), dtype=float)

    errOldModern = zeros(len(h1), dtype=float)
    errMiddleModern = zeros(len(h1), dtype=float)


    i = 0;
    for (v1, v2, v3, b1, b2, b3) in zip(h1, h2, h3, e1, e2, e3):
        # The decay rate is simply the logarithmic difference between the two Tang distributions
        decayOldModern[i] = log10(v1 / v3);
        decayMiddleModern[i] = log10(v1 / v2);

        # The errors are straightforward
        errOldModern[i] = log10((1+b1/v1) / (1-b3/v3));
        errMiddleModern[i] = log10((1+b1/v1) / (1-b2/v2));
        i = i+1

    # We fit the slopes in a loglog scale for the relevant points the (begDecay-endDecay) bins
    [slopeOld, interceptOld] = polyfit(log10(centers[begDecay:endDecay]), log10(-decayOldModern[begDecay:endDecay]), 1)
    [slopeMiddle, interceptMiddle] = polyfit(log10(centers[begDecay:endDecay]), log10(-decayMiddleModern[begDecay:endDecay]), 1)

    # The fitted lines
    simulOld = decayOldModern
    simulMiddle = decayMiddleModern

    for i,c in enumerate(centers):
        simulOld[i] = -10**(slopeOld*log10(c) + interceptOld);
        simulMiddle[i] = -10**(slopeMiddle*log10(c) + interceptMiddle);

    return [decayOldModern, decayMiddleModern, errOldModern, errMiddleModern, simulOld, simulMiddle, slopeOld, slopeMiddle]




def getDecayFromFrequencySlices(edges, centers, sourcedIrregulars):
    # We return the decay rates and error bars as computed below.

    tang = getTangDistributions(edges, sourcedIrregulars)
    [miniF, maxiF] = getErrorForFrequencySlices(edges, centers, sourcedIrregulars)

    [fittedSlices, [slopesOfFreqSlices, x, y, sl,intercept]] = computeDecayFromFrequencySlices(centers, tang)
    bars = zeros(len(miniF), dtype=float)

    for i, (m, M, s) in enumerate(zip(miniF, maxiF, slopesOfFreqSlices)):
        bars[i] = max(M+s, -s-m)

    return[fittedSlices,[slopesOfFreqSlices, x, y, sl, intercept], bars]




def getErrorForFrequencySlices(edges, centers, sourcedIrregulars):
    # We account for Poisson error in the measurement of frequencies

    [h1, h2, h3] = getTangDistributions(edges, sourcedIrregulars)
    [b1, b2, b3] = computeTangErrors(edges, sourcedIrregulars)

    maxiF = zeros(len(h1), dtype=float)
    miniF = 100000*ones(len(h1), dtype=float)
    a = [-1, 1]
    for i in a:
        for j in a:
            for k in a:
                tg1 = zeros(len(h1), dtype=float)
                tg2 = zeros(len(h2), dtype=float)
                tg3 = zeros(len(h3), dtype=float)
                l = 0;
                for (v1, v2, v3, e1, e2, e3) in zip(h1, h2, h3, b1, b2, b3):
                    tg1[l] = v1 + i*e1;
                    tg2[l] = v2 + j*e2;
                    tg3[l] = v3 + k*e3;
                    l = l+1
                [dum, [f, dum, dum, dum, dum]] = computeDecayFromFrequencySlices(centers, [tg1, tg2, tg3])
                for ll, (ff, m, M) in enumerate(zip(f, miniF, maxiF)):
                    miniF[ll] = min(m, -ff)
                    maxiF[ll] = max(M, -ff)

    return [miniF, maxiF]




def computeDecayFromFrequencySlices(centers, tangDistributions):
    # We compute the decay in a time-dependant fashion, as shown in Figure 2

    [h1, h2, h3] = tangDistributions;
    fittedUpperLim = [1.1, 1.2, 1.2, 1.2]
    fittedLowerLim = [0.85, 0.75, 0.75, 0.75]

    slopesOfFreqSlices = zeros(len(centers), dtype=float);
    fittedSlices = []
    l =  zeros(len(centers), dtype=float);  # Log of the centers
    ls =  zeros(len(centers), dtype=float); # Log of the slopes

    i = 0;
    for (fll,ful, v1, v2, v3) in zip(fittedLowerLim, fittedUpperLim, h1, h2, h3):
        freqSlice = [log10(v3), log10(v2), log10(v1)];
        if (v1>0):
            [slope, dum] = polyfit(years, freqSlice, 1);
        else:
            [slope, dum] = polyfit(years[:2], freqSlice[:2], 1);

        # This slope is the decay rate for this frequency slice?
        slopesOfFreqSlices[i] = slope

        x = [years[0]*fll, years[2]*ful];
        fittedSlice = [x, [log10(centers[i]), log10(centers[i])], [x[0]*slope+dum, x[1]*slope+dum]];
        fittedSlices.append(fittedSlice)
        l[i] = log10(centers[i])
        ls[i] = log10(-slope)
        i = i+1

    # We compute the fit for the slope. y is the fitted line on times x
    [sl, intercept] = polyfit(l[begFreq:endFreq], ls[begFreq:endFreq], 1)
    x = [centers[begFreq]*0.5, centers[endFreq-1]*1.5];
    y = [10**(log10(x[0])*sl+intercept), 10**(log10(x[1])*sl+intercept)]

    return [fittedSlices, [slopesOfFreqSlices, x, y, sl, 10**intercept]]

def evolveDistribution(centers, distribution, decayExponent, decayIntercept, time):
    decay = zeros(len(centers), dtype=float);
    for i, c in enumerate(centers):
        decay[i] = decayIntercept*(c**decayExponent);

    evolved = zeros(len(distribution), dtype=float)
    for k, (c,e, d) in enumerate(zip(centers, distribution, decay)):
            evolved[k] = 10**(log10(e) - d*time);
    return evolved

def predictNumerosity(centers, distribution, decayExponent, decayIntercept, time):
    evolved = evolveDistribution(centers, distribution, decayExponent, decayIntercept, time)
    return sum(evolved)

#------------------------------------------------------------------------------#
#------------------------   PERIODS OF DECAY    -------------------------------#
#------------------------------------------------------------------------------#


def computeDecayTime(decayExponent, decayIntercept, frequency, proportion):
    # The time needed for a given frequency slice under evolutionary decay
    # to regularize a given proportion of its irregular verbs

    p = decayIntercept*(frequency**decayExponent);
    return -log10(proportion)/p


def computeRegularizedProportion(decayExponent, decayIntercept, frequency, time):
    # The proportion of verbs from a given frequency slice that will be regularized
    # during a given time

    p = decayIntercept*(frequency**decayExponent);
    return 10**(-p*time)


def getEquivalentDecay(decayExponent, decayIntercept, frequencyBase, frequencyTarget, numberBase, numberTarget):
    # The number of verbs from a target frequency slice that will be regularized during
    # the time it takes for ONE verb from the base frequency slice to be regularized

    t = computeDecayTime(decayExponent, decayIntercept, frequencyBase, float(numberBase-1)/float(numberBase))
    p = computeRegularizedProportion(decayExponent, decayIntercept, frequencyTarget, t)
    return float(numberTarget)*(1-p)




#------------------------------------------------------------------------------#
#-----------------------------     ZIPF'S LAW  --------------------------------#
#------------------------------------------------------------------------------#



def fitBestZipf(centers, newCenters, tang, slope, intercept, realYears, yearsToTry):
    # Finding the straightest distribution all three curves originate from

    goodness = zeros(len(yearsToTry), dtype=float)
    slopes = zeros(len(yearsToTry), dtype=float)
    intercepts = zeros(len(yearsToTry), dtype=float)
    ax = []

    # Given a timelapse, and given the decay (in argument), we compute the distribution of
    # irregular verbs that led to the three observed curves in Modern, Middle and Old lish.
    for k,tt in enumerate(yearsToTry):

        timeLapses = realYears+tt

        l = zeros((len(tang), len(tang[0])), dtype=float);
        for i,d in enumerate(tang):
            for j,c in enumerate(centers):
                l[i,j] = intercept*(c**slope)* timeLapses[i] + log10(d[j]);
        s = mean(l)

        [slopeZ, dum] = polyfit(log10(centers[begEvol:endEvol]), s[begEvol:endEvol], 1)
        v = polyval([slopeZ, dum], log10(centers));
        goodness[k] = norm(v[begEvol:endEvol]-s[begEvol:endEvol])
        slopes[k] = slopeZ;
        intercepts[k] = dum;

    # The timelapse that yields the straighter distribution is taken as the origin of times
    origin = find(goodness==min(goodness))

    # We compute the original distribution of irregular verbs
    zipf = polyval([slopes[origin], intercepts[origin]], log10(newCenters))
    print("Slope of the original Zipf Law: " + str(slopes[origin]))

    return [zipf, -yearsToTry[k], [slopes[k], intercepts[k]]]



def getBestMesh(centers, distributions, decayExponent, decayIntercept, times, zeroes):
    # We return the 3D mesh computed in the next function

    newCenters = linspace(min(log10(centers)), -0.5, samplingOfFrequency);
    [zipf, origin, p] = fitBestZipf(centers, 10**newCenters,  distributions, decayExponent, decayIntercept, zeroes, times);

    # We produce the mesh
    timesToPlot = linspace(origin, endTimes, samplingOfTime)
    m = computeEvolvedMesh(10**newCenters, 10**zipf, decayExponent, decayIntercept, timesToPlot, origin)

    return m



def computeEvolvedMesh(centers, distribution, decayExponent, decayIntercept, times, zero):
    # We compute the mesh of the linguistic surface using the decay parameters computed in Figure 2

    curves = zeros(len(times))
    meshX = zeros((len(times), len(centers)), dtype=float)
    meshY = zeros((len(times), len(centers)), dtype=float)
    meshZ = zeros((len(times), len(centers)), dtype=float)

    logDistribution = log10(distribution)

    for k, dum in enumerate(centers):
        meshX[:,k] = times

    # computing the decay rates
    decay = zeros(len(centers), dtype=float);
    for i, c in enumerate(centers):
        decay[i] = decayIntercept*(c**decayExponent);

    for i,t in enumerate(times):
        # Evolving the distribution
        for k, (c,e, d) in enumerate(zip(centers, distribution, decay)):
            meshY[i, k] = log10(c);
            meshZ[i, k] = log10(e) - d*(t-zero);

            if meshZ[i, k] < 0:
                meshZ[i,k] = 0


    return [meshX, meshY, meshZ]



#------------------------------------------------------------------------------#
#----------------------------   PLOTS  ----------------------------------------#
#------------------------------------------------------------------------------#




def plotTangDistributions(sourced, xlims):
    # First panel, figure 1
    # The optional arguments allow us to correct the binning algorithm for low numbers by hand.

    # Gathering data
    frequs = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequs[0]);
    res = getTangDistributions(edges, sourced)

    ax = axes([0.12, 0.46, 0.8, 0.5])

    # Plotting lines
    semilogx(centers, res[2],marker='o', color='g', ms=markersize, mec='g')
    semilogx(centers, res[1],marker='o', color='r', ms=markersize, mec='r')
    semilogx(centers, res[0],marker='o', color='b', ms=markersize, mec='b')

    (errorTangOld, errorTangMid, errorTangMod, errorDecaysOld, errorDecaysMid,
            meanDecayOld, meanDecayMid, stdevDecayOld, stdevDecayMid, errorSlopesOfFreqSlices) = computeTangSamplingError(edges, centers, sourced)

    ax.errorbar(centers, res[2], errorTangOld, color='g')
    ax.errorbar(centers, res[1], errorTangMid, color='r')
    ax.errorbar(centers, res[0], errorTangMod, color='b')

    # Make pretty
    ax.set_xlim(xlims[0], xlims[1])
    ax.set_xlabel('Frequenz')
    ax.set_ylabel(u'Anzahl irregulärer Verben')

    # Position of legend -- adjust as need be for appearance, check against next func.
    ax.legend(('Alt'+glanguage, 'Mittel'+glanguage, 'Neu'+glanguage), loc=(0.55, 0.8)) #original val: (0.65,0.8)
    leg = ax.get_legend()
    leg.draw_frame(False)

    return ax

def plotTangDistributionsNoErrors(sourced, xlims, correctEdges=False, posToCorrect=[0], valueToCorrect=[0]):
    # First panel, figure 1
    # The optional arguments allow us to correct the binning algorithm for low numbers by hand.

    # Gathering data
    frequs = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequs[0], correctEdges, posToCorrect, valueToCorrect);
    res = getTangDistributions(edges, sourced)

    ax = axes([0.12, 0.46, 0.8, 0.5])

    # Plotting lines
    semilogx(centers, res[2],marker='o', color='g', ms=markersize, mec='g')
    semilogx(centers, res[1],marker='o', color='r', ms=markersize, mec='r')
    semilogx(centers, res[0],marker='o', color='b', ms=markersize, mec='b')

    # Make pretty
    ax.set_xlim(xlims[0], xlims[1])
    ax.set_xlabel('Frequenz')
    ax.set_ylabel(u'Anzahl irregulärer Verben')

    # Position of legend -- adjust as need be for appearance, check against prev. func
    ax.legend(('Alt', 'Mittel', 'Neu'), loc=(0.55, 0.8)) #original val: (0.65,0.8)
    leg = ax.get_legend()
    leg.draw_frame(False)

    return ax



def plotDecayRates(sourced, xlims, correctEdges=False, posToCorrect=0, valueToCorrect=0):
    #First Panel, figure 2

    # Gathering data
    frequs = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequs[0], correctEdges, posToCorrect, valueToCorrect);
    res = computeDecayRates(edges, centers, sourced)
    del(frequs)

    # Make the axes
    ax = axes([0.12, 0.06, 0.8, 0.3])

    # Plot the lines
    ax.loglog(centers[begDecay:endDecay], -res[0][begDecay:endDecay], marker='o', color='g', mec='g', ms=markersize)
    ax.loglog(centers[begDecay:endDecay], -res[1][begDecay:endDecay], marker='o', color='r', mec='r', ms=markersize)

    ax.loglog(centers[begDecay:endDecay], -res[4][begDecay:endDecay], color='g', linestyle='--', mec='g', ms=markersize)
    ax.loglog(centers[begDecay:endDecay], -res[5][begDecay:endDecay],  color='r', linestyle='--', mec='r', ms=markersize)

    (errorTangOld, errorTangMid, errorTangMod, errorDecaysOld, errorDecaysMid,
            meanDecayOld, meanDecayMid, stdevDecayOld, stdevDecayMid,errorSlopesOfFreqSlices) = computeTangSamplingError(edges, centers, sourced)

    ax.errorbar(centers[begDecay:endDecay], -res[0][begDecay:endDecay], errorDecaysOld[begDecay:endDecay], color='g')
    ax.errorbar(centers[begDecay:endDecay], -res[1][begDecay:endDecay], errorDecaysMid[begDecay:endDecay], color='r')


    # Set the labels
    ax.set_xlabel('Frequenz')
    ax.set_ylabel('Regularisierungsrate')

    # Position of legend -- adjust as need be for appearance, check againgst next func.
    ax.legend(('vom Alt- zu Neu'+glanguage+'en', 'vom Mittel- zu Neu'+glanguage+'en'), loc=(0.38, 0.75)); #original val: (0.5,0.75)
    leg = ax.get_legend()
    leg.draw_frame(False)
    ax.set_ylim(0.02, 2)
    ax.set_xlim(xlims[0], xlims[1])

    t1 = text(0.4, 0.66, str(res[6])[:5], horizontalalignment='center',
         verticalalignment='center',
         transform = ax.transAxes,
         fontsize=13,
         color='g')

    t2 = text(0.28, 0.40, str(res[7])[:5], horizontalalignment='center',
         verticalalignment='center',
         transform = ax.transAxes,
         fontsize=13,
         color='r')

    return (ax, t1, t2)


def plotDecayRatesNoErrors(sourced, xlims, correctEdges=False, posToCorrect=0, valueToCorrect=0):
    #First Panel, figure 2

    # Gathering data
    frequs = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequs[0], correctEdges, posToCorrect, valueToCorrect);
    res = computeDecayRates(edges, centers, sourced)
    del(frequs)
    del(edges)

    # Make the axes
    ax = axes([0.12, 0.06, 0.8, 0.3])

    # Plot the lines
    ax.loglog(centers[begDecay:endDecay], -res[0][begDecay:endDecay], marker='o', color='g', mec='g', ms=markersize)
    ax.loglog(centers[begDecay:endDecay], -res[1][begDecay:endDecay], marker='o', color='r', mec='r', ms=markersize)

    ax.loglog(centers[begDecay:endDecay], -res[4][begDecay:endDecay], color='g', linestyle='--', mec='g', ms=markersize)
    ax.loglog(centers[begDecay:endDecay], -res[5][begDecay:endDecay],  color='r', linestyle='--', mec='r', ms=markersize)

    # Set the labels
    ax.set_xlabel('Frequenz')
    ax.set_ylabel('Regularisierungsrate')

    # Position of legend -- adjust as need be for appearance, check againgst next func.
    ax.legend(('vom Alt- zu Neu'+glanguage+'en', 'vom Mittel- zu Neu'+glanguage+'en'), loc=(0.38, 0.75)); #original val: (0.5,0.75)
    leg = ax.get_legend()
    leg.draw_frame(False)
    ax.set_ylim(0.02, 2)
    ax.set_xlim(xlims[0], xlims[1])

    t1 = text(0.4, 0.66, str(res[6])[:5], horizontalalignment='center',
         verticalalignment='center',
         transform = ax.transAxes,
         fontsize=11,
         color='g')

    t2 = text(0.28, 0.40, str(res[7])[:5], horizontalalignment='center',
         verticalalignment='center',
         transform = ax.transAxes,
         fontsize=11,
         color='r')

    return (ax, t1, t2)


def plot3DIrregularDistributions(sourced, fig):
    # Second Panel, figure 1

    # Gathering data
    frequs = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequs[0]);
    [modern, middle, old] = get3DIrregularDistribution(edges, centers, sourced)
    [freqSlices, slopes , bars] = getDecayFromFrequencySlices(edges, centers, sourced)

    # Making axes
    ax = p3.Axes3D(fig, [0, 0.4, 1, 0.6], azim = angle2[0], elev = angle2[1])

    # Defining styles
    style1 = '-';
    width1=2;
    ms1=6;
    w = 2
    w2 = 1
    colo = '#657383'
    colo = 'k'
    style2='--'

    #Plotting lines
    for f in freqSlices[:4]:
        ax.plot3D(f[0],f[1], f[2], color=colo, ls=style2, linewidth=w2)

    ax.plot3D(old[0], old[1], old[2], marker='o', color='g', ls = style1, linewidth=width1, ms=ms1, mec='g')
    ax.plot3D(middle[0], middle[1], middle[2], marker='o', color='r', ls = style1, linewidth=width1, ms=ms1, mec='r')
    ax.plot3D(modern[0], modern[1], modern[2], marker='o', color='b', ls = style1, linewidth=width1, ms=ms1, mec='b')


    # Make pretty
    #ax.set_xticks([800, 1200, 2000])
    ax.set_xticks(years)
    ax.set_zticks([0, 1, 1.8])
    ax.set_yticks(log10(centers))
    ax.set_zticklabels(['1', '10', '60'])
    ax.set_yticklabels([r'$10^{-6}$', r'$10^{-5}$',r'$10^{-4}$',r'$10^{-3}$',r'$10^{-2}$',r'$10^{-1}$','$1$'])

    ax.set_zlim((-0.2, 2))
    ax.set_xlabel('\n \n Zeit (Jahre n.Chr.)')
    ax.set_ylabel('\n Frequenz')
    ax.set_zlabel(u'Anzahl irregulärer Verben \n \n')

    return ax



def plotDecayFromFrequencies(sourced, fig):
    # Second Panel, figure 2

    # Gathering data
    frequs = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequs[0]);
    [freqSlices, slopes, bars] = getDecayFromFrequencySlices(edges, centers, sourced)
    (errorTangOld, errorTangMid, errorTangMod, errorDecaysOld, errorDecaysMid,
            meanDecayOld, meanDecayMid, stdevDecayOld, stdevDecayMid,errorSlopesOfFreqSlices) = computeTangSamplingError(edges, centers, sourced)


    # Making axes
    ax = axes([0.17, 0.06, 0.7, 0.3])
    msize=4
    style1 = '-';
    width1=2;
    ms1=6;
    w = 2
    colo = '#657383'
    style2='--'

    # Plotting lines
    #for c, f, b in zip([centers[begFreq:endFreq]], [slopes[0][begFreq:endFreq]], [bars[begFreq:endFreq]]):
    for c, f, b in zip([centers[begFreq:endFreq]], [slopes[0][begFreq:endFreq]], [errorSlopesOfFreqSlices[begFreq:endFreq]]):
        ax.loglog(c, -f, marker='o', color='b', mec='b', ms=msize)
        ax.errorbar(c, -f, b, color='b')
    ax.loglog(slopes[1], slopes[2], color='b', ls='--')

    # Make pretty
    ax.set_xlabel('Frequenz')
    ax.set_ylabel('Regularisierungsrate')
    ax.set_xlim(2*10**(-6), 10**(-2))
    ax.set_ylim(10**(-5), 10**(-2))

    text(0.7, 0.6, 'Anstieg: ' + str(slopes[3])[:5], horizontalalignment='center',
         verticalalignment='center',
         transform = ax.transAxes,
         fontsize=13,
         color='b')



def plotTheBestMesh(sourced, times, zeroes, fig):
    # Third Panel

    frequs = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequs[0]);
    [freqSlices, [dum, dum, dum, decayExponent, decayIntercept], bars] = getDecayFromFrequencySlices(edges, centers, sourced)
    tang = getTangDistributions(edges, sourced)

    [meshX, meshY, meshZ] = getBestMesh(centers, tang, decayExponent, decayIntercept, times, zeroes)

    ax = p3.Axes3D(fig, azim=angle3[0], elev=angle3[1])
    ax.plot_wireframe(meshX,meshY,meshZ, colors='#657383')

    return ax



def plotTimeSlicesOverMesh(sourced, ax):

    # Gathering data
    frequs = getFrequencies(sourced);
    [edges, centers] = getHistogramParameters(frequs[0]);
    [modern, middle, old] = get3DIrregularDistribution(edges, centers, sourced)


    # Defining styles
    style1 = '-';
    width1=2;
    ms1=6;

    #Plotting lines
    ax.plot3D(old[0], old[1], old[2], marker='o', color='g', ls = style1, linewidth=width1, ms=ms1, mec='g')
    ax.plot3D(middle[0], middle[1], middle[2], marker='o', color='r', ls = style1, linewidth=width1, ms=ms1, mec='r')
    ax.plot3D(modern[0], modern[1], modern[2], marker='o', color='b', ls = style1, linewidth=width1, ms=ms1, mec='b')


    # Make pretty
    #ax.set_xticks([800, 1200, 2000])
    ax.set_xticks(years)
    ax.set_zticks([0, 1, 1.8])
    ax.set_yticks(log10(centers))
    ax.set_zticklabels(['1', '10', '60'])
    ax.set_yticklabels([r'$10^{-6}$', r'$10^{-5}$',r'$10^{-4}$',r'$10^{-3}$',r'$10^{-2}$',r'$10^{-1}$','$1$'])
    #ax.set_xticklabels([' 800', ' 1200', ' 2000'])
    ax.set_xticklabels(yearsstring)

    ax.set_zlim((0, 4))
    ax.set_ylim((-5.5, -0.2))

    ax.set_xlabel('\n \n Zeit (Jahre n.Chr.)')
    ax.set_ylabel('\n Frequenz')
    ax.set_zlabel(u'Anzahl irregulärer Verben \n \n')





#------------------------------------------------------------------------------#
#-------------------------         I/O        ---------------------------------#
#------------------------------------------------------------------------------#


def readVerbsFile(fileIn):
    # fileIn is the name of the verb file to read

    f = open(fileIn, 'r');

    verb = [];
    frequency = [];
    modernIrregular = [];
    middleIrregular = [];
    oldIrregular = [];
    middleSource = [];
    oldSource = [];

    # Reading the size of the corpus
    s = f.readline();
    sc = s.split("\t")
    global sizeCorpus
    sizeCorpus = int(sc[8])
    # print sizeCorpus

    s = f.readline();
    while s!= '':
        mots = s.split('\t')
        verb.append(mots[0]);
        fl = float(mots[1])
        # print s
        frequency.append(float(fl)/float(sizeCorpus));      # we compute the frequencies from CELEX count
        modernIrregular.append(int(mots[2]));
        middleIrregular.append(int(mots[3]));
        oldIrregular.append(int(mots[4]));
        #print s


        if (len(mots)>6):
            middleSource.append(mots[5]);
            oldSource.append(mots[6]);
        else:
            middleSource.append("NO SOURCE")
            oldSource.append("NO SOURCE")
        s = f.readline()

    res = zip(verb, frequency, modernIrregular, middleIrregular, oldIrregular, middleSource, oldSource)
    return res


def writeTexSupplementaryTable(fileTexSuppTable, res):
    # Outputs the supplementary table of all the verbs used in the study, in tex format

    f = open(fileTexSuppTable, 'w')

    f.write("\\begin{longtable}{c c c c c} \n")
    f.write("\\caption{Complete Data} \\\\ \n")
    f.write("\\toprule \n")
    f.write("\\textbf{Verb} & \\textbf{Alt} & \\textbf{Mittel} & \\textbf{Neulish} & \\textbf{Frequency} \\\\ \n")
    f.write("\\midrule \n \\endfirsthead \n")
    f.write("\\caption{(\\textit{cont.}) Complete Data?} \\\\ \n")
    f.write("\\toprule \n")
    f.write("\\textbf{Verb} & \\textbf{Alt} & \\textbf{Mittel} & \\textbf{Neulish} & \\textbf{Frequency} \\\\ \n")
    f.write("\\midrule \n \\endhead %longtable command for general heading \n ")
    f.write("\\bottomrule \n")
    f.write("\\endfoot \n")
    f.write("\\bottomrule \\endlastfoot \n")

    index = 0
    for i, (verb, frequency, modernIrregular, middleIrregular, oldIrregular, oldSource, middleSource) in enumerate(res):
        num = i/numLinesSuppTable

        s = verb + " & "
        s = s + str(oldIrregular)
        #if not(oldSource == 'N'):
        #    s = s + "$^{" + str(oldSource) + "}$ & "
        #else:
        s = s  + " & "

        s = s + str(middleIrregular)
        #if not(middleSource[0] == 'N'):
        #    s = s + "$^{" + str(middleSource) + "}$ & "
        #else:
        s = s + " & "

        s = s + str(modernIrregular) + " & "

        # Getting the frequency in the right format
        # print s
        expo = int(floor(log10(frequency)))
        reste = frequency/10**(expo)


        s = s + "$" + str(reste)[:4] + " \cdot 10^{" + str(expo) + "}$ "
        s = s + " \\\\"
        f.write(s)
        f.write("\n")

    f.write("\\end{longtable} \n")


def writeTexVerbsTable(fileTexVerbsTable, sourced, bins, decayExponent, decayIntercept, centers):
    # Outputs the Table 1 of the paper, in tex format
    # Note that some manual formatting needs to be done to produce exactly the Table 1

    f = open(fileTexVerbsTable, 'w')

    # f.write("The Irregular Verbs project. Supplementary table. \n")
    # f.write("Erez Lieberman, Jean-Baptiste Michel, February 2007")
    # f.write("\n \n")

    f.write("\\begin{tabular}{c p{8cm} c c} \n")
    f.write("\\toprule \n")
    s = "\\textbf{Frequenz} & \\textbf{Verben} & \\textbf{\\% Reg} & \\textbf{Halbwertzeit} \\\\ \n"
    f.write(s)
    f.write("\\midrule \n")
    numchars = 40
    s = []
    for i in linspace(0, max(bins), 1+max(bins)):
        s.append("");

    lenf = zeros(max(bins)+1)
    reg = zeros(max(bins)+1)
    irr = zeros(max(bins)+1)
    sumf = zeros(max(bins)+1, dtype=float)
    numV = zeros(max(bins)+1, dtype=float)

    for i, bb in enumerate(bins):
        b = int(max(bins) - bb)
        v = sourced[i][0]
        mod = sourced[i][2]

        if mod==0:
            st = " {\\color{red} " + v + "}"
            reg[b] = reg[b] + 1
        else:
            st = " " + v
            irr[b] = irr[b] + 1

        sumf[b] = sourced[i][1] + sumf[b]
        numV[b] = numV[b] + 1;


        s[b] = s[b] + st + ","
        lenf[b] = lenf[b] + len(v)

    meanf = [];
    for i,ss in enumerate(sumf):
        meanf.append(ss/numV[i])

    for i, m in enumerate(s):
        c = centers[size(centers) - i - 1]
        thalf = computeDecayTime(decayExponent, decayIntercept, c , 0.5)
        mo = int(floor(log10(meanf[i])))

        f.write("$10^{" + str(mo) + "} - 10^{" + str(mo+1) + "}$  & \\textit{ ")
        f.write(m + "} & ")
        f.write(str(int(100*float(reg[i])/float(reg[i]+irr[i])))[:3] + " & ")
        f.write(str(int(thalf)) + "\\\\ \n")

    f.write("\\bottomrule \n")
    f.write("\\end{tabular}")



#------------------------------------------------------------------------------#
#--------------------------       Execute      --------------------------------#
#------------------------------------------------------------------------------#


# Setup graphic parameters
params = {'axes.labelsize': axeslabelsize,
        'legend.fontsize': legendfontsize,
        'xtick.labelsize': xticklabelsize,
        'ytick.labelsize': yticklabelsize}
pylab.rcParams.update(params)

# Run the code
execute()