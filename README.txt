Here we provide the Python source code and the data that we used to produce Figure 1, 2, and 3, and Table 1. The code requires Python 2.4.1, the Matplotlib graphic library and the graphic backend WXAgg. Matplotlib and WXAgg are still under development, and some fixes need to be made for our code to run smoothly. The source code is distributed under the GNU Public License.

Below, we describe how to install Enthought Python, a Python distribution containing all the required modules. We specify exactly how to make the required changes to the libraries by replacing certain files with ones which we have provided. Finally, we show how to run the Python code to reproduce our results. The commented source code can also be browsed to clarify our exact methods.

							Erez Lieberman, Jean-Baptiste Michel, June 2007


----------------------------------------------------------------------------------
A) Installing Python

1. If necessary, disable any previously existing Python distributions. 
In particular, remove the following environment variables (in Control Panel\System Properties\Advanced)
PYTHON CASE
PYTHON PATH

2. Download Enthought Python 2.4.1 from the website:
http://code.enthought.com/downloads/enthon-python2.4-1.0.0.exe

3. Install python:
Run the downloaded file and follow the on-screen instructions.


B) Correcting bugs

Let "$python_folder" be the path to your newly installed python distribution (e.g., "C:\Python24")

4. Fixing Matplotlib
Copy all the files from the folder "copy into matplotlib" into the folder "$python folder\Lib\sites-packages\matplotlib\".

Files with those names already exist in the destination folder, so in this step you will need to replace them. Those files are:
"axes3d.py", "axes.py", "axis3D.py", "axis.py", "legend.py", "text.py", "pylab.py", "collections.py", "lines.py", "ticker.py" and "artist.py"

5.  Fixing WXAgg
Copy the files from the folder "copy into backends" into "$python folder\Lib\sites-packages\matplotlib\backends\".
Again, files with the same name already exists, so you must replace them. The files to copy should be:
"backend_wxagg.py" and "backend_wx.py"


C) Running the code

6. Configure Matplotlib
Copy the file "matplotlibrc" from the folder "copy into home" into "Documents and Settings\your_user_name\.matplotlibrc".
If the folder ".matplibrc" does not already exist in your home folder, you should create it.

7. Run the code
- Open IDLE, the graphic interface to python.
- From IDLE, open the file "Lieberman-Michel IrregularVerbs.py" by navigating to the appropriate folder. Make sure the data 
  file "Lieberman-Michel_english_verbs.txt" is in the same directory as the python file.
- Launch the code using the F5 key, or through the menu "Run \ Run Module". The figures will appear on screen, statistics will be displayed
  in the console and files will be created containing the tables and the figures.